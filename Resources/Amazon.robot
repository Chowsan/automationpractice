*** Settings ***
Resource  ../PageObjects/ProductsPage.robot
Resource  ../PageObjects/LandingPage.robot
Resource  ../PageObjects/TopNavigationPage.robot

*** Keywords ***
Search for Products
    LandingPage.Load the url
    LandingPage.Verify page is loaded
    TopNavigationPage.Enter product name to search
    TopNavigationPage.Search the product
Select Product from Search Results
    ProductsPage.Select first product
    ProductsPage.Verify product details page is displayed
Add Product to cart
    ProductsPage.Add to List
Verify product details
    ProductsPage.Verify sign in page is displayed
