*** Settings ***
Library  SeleniumLibrary

*** Keywords ***
Select first product
    click link  xpath=//a[contains(@title,'Ferrari Scuderia Red Eau De Toilette Spray, 4.2 oz')]
Verify product details page is displayed
    wait until page contains  Back to search results
Add to List
    click button  xpath=//input[@title='Add to List']
Verify sign in page is displayed
    page should contain textfield  xpath=//input[@id='ap_email']