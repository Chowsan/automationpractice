*** Settings ***
Library  SeleniumLibrary
*** Variables ***
${TOPNAV_SEARCH_BAR} =      id=twotabsearchtextbox

*** Keywords ***
Enter product name to search
    input text  ${TOPNAV_SEARCH_BAR}    ${SEARCH_ITEM}
Search the product
    click button  xpath=(//input[@value='Go'])[1]