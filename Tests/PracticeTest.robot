*** Settings ***
Documentation  First Robot Script
Resource  ../Resources/Common.robot
Resource  ../Resources/Amazon.robot
Suite Setup  Common.Begin Web Test
Suite Teardown  Common.End Web Test

*** Variables ***
${LOGIN URL}    https://www.amazon.com/
${SEARCH_ITEM}  ferrari perfume for men
${BROWSER}      chrome
${GLOBALVAR}    GLobal
*** Test Cases ***
User select a product from amazon and checkout
    [Tags]  Gherkin

    log to console  ${newvar}
    Given Search for Products
    And Select Product from Search Results
    When Add Product to cart
    Then Verify product details

User select a product from amazon and checkout
    [Tags]  Regression  Smoke

    Amazon.Search for Products
    Amazon.Select Product from Search Results
    Amazon.Add Product to cart
    Amazon.Verify product details